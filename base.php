<?php get_template_part('templates/head'); ?>
<body <?php body_class(); ?>>

<div class="ribbon visible-xs" style="text-align: left; right: 25px; background-color: #b80b19;"><a rel="me" href="#BS">XS<br/><span class="scrnsize"></span></a></div>
      <div class="ribbon visible-sm" style="text-align: left; right: 25px; background-color: #ba7222;"><a rel="me" href="#BS">SM<br/><span class="scrnsize"></span></a></div>
      <div class="ribbon visible-md" style="text-align: left; right: 25px; background-color: #dcdc28;"><a rel="me" href="#BS">MD<br/><span class="scrnsize"></span></a></div>
      <div class="ribbon visible-lg" style="text-align: left; right: 25px; background-color: #69c022;"><a href="#BS">LG<br/><span class="scrnsize"></span></a></div>

  <div class="container" style="padding:0 15px; max-width: 100%;">
<!-- <div class="row"> -->

  <!--[if lt IE 8]>
    <div class="alert alert-warning">
      <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'roots'); ?>
    </div>
    <![endif]-->

    <?php
    do_action('get_header');
    get_template_part('templates/header-top-navbar');
    ?>

    <!-- <main class="main <?php echo roots_main_class(); ?>" role="main"> -->
    <?php include roots_template_path(); ?>
    <!-- </main>/.main -->
    <?php if (roots_display_sidebar()) : ?>
      <aside class="sidebar <?php echo roots_sidebar_class(); ?>" role="complementary">
        <?php include roots_sidebar_path(); ?>
      </aside><!-- /.sidebar -->
    <?php endif; ?>

  <?php get_template_part('templates/footer'); ?>

</div>
</body>
</html>
