<?php if (is_front_page())  : ?>

<div class="headerimg frontpage col-sm-12">
     <!-- <div class="form-wrapper"> -->
     <div class="col-sm-6 col-sm-offset-6" style="background-color: #D8DDDB;">
     Insert a Quote here
     </div>

     <form class="squeeze" role="form" accept-charset="UTF-8" action="https://bf117.infusionsoft.com/app/form/process/d37eb8ad9daeb7525df2b3df064af50b" method="POST">
<input name="inf_form_xid" type="hidden" value="d37eb8ad9daeb7525df2b3df064af50b" />
    <input name="inf_form_name" type="hidden" value="Site Squeeze Page" />
    <input name="infusionsoft_version" type="hidden" value="1.29.8.45" />
      <div class="kw-form-fieldset">
      <div class="kw-form-field">
         <input class="form-control" id="inf_field_FirstName" type="text" name="inf_field_FirstName" placeholder="First" />
      </div>
      <div class="kw-form-field">
         <input class="form-control" id="inf_field_LastName" type="text" name="inf_field_LastName" placeholder="Last" />
      </div>
      <div class="kw-form-field">
         <input class="form-control" id="inf_field_Email" type="email" name="inf_field_Email" placeholder="Email" />
      </div>
      <div class="kw-form-field">
         <input class="btn btn-default btn-circle" type="submit" value="GO" />
      </div>
      </div>
   </form>
   <!-- </div> -->
<div class="squeezecontact" style="display:none;">
<a href="#contact"><span>Contact Us</span>
<img src="/content/themes/roots/assets/img/icons/contact.png"/>
</a>
</div>
   </div>



<?php else: ?>


<div class="page-header">
  <h1>
    <?php echo roots_title(); ?>
  </h1>
</div>

<?php endif; ?>
