<?php 
global $wp_query;

$template_name = get_post_meta( $wp_query->post->ID, '_wp_page_template', true )
?>

<?php if ($template_name != "template-squeeze.php") : ?>
<nav class="navbar navbar-default" role="navigation">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <!-- <a class="navbar-brand" href="#">Brand</a> -->
    <div class="kwsign banner" >
    <div class="bigk">
      K
    </div>
    <div class="littlekwletters">
      risstina
    </div>
    <div class="bigw">
      W
    </div>
    <div class="littlekwletters">
      ise
    </div>
  </div>
  </div>

  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse pull-right" id="bs-example-navbar-collapse-1">
     <?php
        if (has_nav_menu('primary_navigation')) :
          wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav'));
        endif;
      ?>
  </div><!-- /.navbar-collapse -->
</nav>

<?php else: ?>

<div class="banner col-sm-12">
  <div class="kwsign" >
    <div class="bigk">
      K
    </div>
    <div class="littlekwletters">
      risstina
    </div>
    <div class="bigw">
      W
    </div>
    <div class="littlekwletters">
      ise
    </div>
  </div>
  </div>
<?php endif; ?>
     
