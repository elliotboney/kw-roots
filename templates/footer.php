<?php 
global $wp_query;

$template_name = get_post_meta( $wp_query->post->ID, '_wp_page_template', true )
?>

<?php if ($template_name == "template-squeeze.php") : ?>
<footer class="content-info container squeeze" role="contentinfo">
  <div class="row">
  <div class="socialwrap">
    <a href="https://www.facebook.com/krisstinawise"><img src="<?php echo get_template_directory_uri() . "/assets/img/icons/fb.png"; ?>"/></a>
    <a href="https://twitter.com/krisstinawise"><img src="<?php echo get_template_directory_uri() . "/assets/img/icons/twitter.png"; ?>"/></a>
    <a href="http://www.linkedin.com/in/krisstinawise"><img src="<?php echo get_template_directory_uri() . "/assets/img/icons/linkedin.png"; ?>"/></a>
    <a href="mailto:krisstina@thegoodlifeteam.com"><img src="<?php echo get_template_directory_uri() . "/assets/img/icons/apple.png"; ?>"/></a>
</div>
  </div>
</footer>
<?php else: ?>


<footer class="content-info container" role="contentinfo">
  <div class="row">
    <div class="col-lg-12">
      <p>&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?></p>
    </div>
  </div>
</footer>
<?php endif; ?>
<?php wp_footer(); ?>
