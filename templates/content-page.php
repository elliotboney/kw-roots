<?php 
global $wp_query;

$template_name = get_post_meta( $wp_query->post->ID, '_wp_page_template', true )
?>

<?php if (is_front_page())  : ?>

<h1>Blah</h1>

<?php else: ?>

<?php while (have_posts()) : the_post(); ?>
  <?php the_content(); ?>
  <?php wp_link_pages(array('before' => '<nav class="pagination">', 'after' => '</nav>')); ?>
<?php endwhile; ?>
<?php endif; ?>


